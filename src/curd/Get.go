package todopkg

import (
	"log"
	"net/http"
	constants "todo/src/const"
	"todo/src/db"

	"github.com/gin-gonic/gin"
)

func GetToDo(c *gin.Context) { // this function will list all itens or single (if id is passed)
	var todoItem constants.ToDoDBStruct
	var allTodos []constants.ToDoDBStruct

	if ok := c.Param("id"); ok != "" {
		// validates if the todoId is valide or not
		if err := db.Connection.Where("id = ?", c.Param("id")).First(&todoItem).Error; err != nil {
			log.Panicln("Bad request, ID passed in the request is not present in database")
			c.JSON(http.StatusNoContent, gin.H{"error": "Record not found!"})
			return
		}
		c.JSON(http.StatusOK, gin.H{"data": todoItem})
	} else {
		db.Connection.Find(&allTodos)
		c.JSON(http.StatusOK, gin.H{"data": allTodos})
	}

}
