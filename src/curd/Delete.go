package todopkg

import (
	"log"
	"net/http"
	constants "todo/src/const"
	"todo/src/db"

	"github.com/gin-gonic/gin"
)

func DeleteToDo(c *gin.Context) {
	var todoItem constants.ToDoDBStruct

	// validates if the todoId is valide or not
	if err := db.Connection.Where("id = ?", c.Param("id")).First(&todoItem).Error; err != nil {
		log.Panicln("Bad request, ID passed in the request is not present in database")
		c.JSON(http.StatusNoContent, gin.H{"error": "Record not found!"})
		return
	}
	// delete entry from database
	db.Connection.Delete(&todoItem)
	log.Println("Deleted  entry from database:\n", todoItem)
	// set the json response for the request
	c.JSON(http.StatusFound, gin.H{"data": true})
}
