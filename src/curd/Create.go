package todopkg

import (
	"log"
	"net/http"
	constants "todo/src/const"
	"todo/src/db"

	"github.com/gin-gonic/gin"
)

func GenerateToDo(c *gin.Context) {
	var todoInput constants.ToDoInput
	// validates if the input is valide or not
	if err := c.ShouldBindJSON(&todoInput); err != nil {
		log.Printf("Bad request, JSON passed in the request is not matching the expected parameters")
		println("AERerer", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// create new object to insert in database
	newTodo := constants.ToDoDBStruct{
		Title:      todoInput.Title,
		DoneStatus: todoInput.DoneStatus,
	}

	// create database entry
	db.Connection.Create(&newTodo)
	log.Println("Created new entry.\n", newTodo)

	//set json response
	c.JSON(http.StatusCreated, gin.H{"data": newTodo})
}
