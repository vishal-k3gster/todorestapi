package todopkg

import (
	"log"
	"net/http"
	constants "todo/src/const"
	"todo/src/db"

	"github.com/gin-gonic/gin"
)

func UpdateToDo(c *gin.Context) {
	var todoInput constants.ToDoInput
	var todoItem constants.ToDoDBStruct

	if err := c.ShouldBindJSON(&todoInput); err != nil {
		log.Printf("Bad request, JSON passed in the request is not matching the expected parameters")
		println("Error..:", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// validates if the todoId is valide or not
	if err := db.Connection.Where("id = ?", c.Param("id")).First(&todoItem).Error; err != nil {
		log.Panicln("Bad request, ID passed in the request is not present in database")
		c.JSON(http.StatusNoContent, gin.H{"error": "Record not found!"})
		return
	}

	// updates the value in database
	db.Connection.Model(&todoItem).Updates(todoInput)
	log.Println("Updated the database entry of todo:\n", todoItem)
	c.JSON(http.StatusOK, gin.H{"data": todoItem})
}
