/*
This package is used for declaration of constants
which will be used through out the project
*/
package constants

type ToDoInput struct { // to be used for curd operations
	Title      string `json:"title"`
	DoneStatus string `json:"DoneStatus"`
}

type ToDoDBStruct struct { // to be used for database operations
	ID         uint   `json:"id" gorm:"primary_key"`
	Title      string `json:"title"`
	DoneStatus string `json:"DoneStatus"`
}
