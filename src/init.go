/*
This is the main package of ToDo app.
It starts initiates the database connection, add routes to the RestAPI server
Starts the server on 8000 post
*/

package main

import (
	"log"
	"os"
	"time"
	todopkg "todo/src/curd"
	"todo/src/db"

	"github.com/gin-gonic/gin"
	cors "github.com/itsjamie/gin-cors"
)

func main() {

	f, err := os.OpenFile("../logs/todo.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Panicf("error opening file: %v", err)
	}
	defer f.Close()
	log.SetOutput(f)
	log.Println("This is a test log entry")

	log.Println("Init process of database connection.")
	db.ConnectDatabase()
	log.Println("Init process of restAPI server.")

	_, fileErr := os.Create("../logs/server.log")
	if fileErr != nil {
		log.Panic(fileErr)
	}
	// gin.DefaultWriter = file
	router := gin.Default()
	router.Use(cors.Middleware(cors.Config{
		Origins:         "*",
		Methods:         "GET, PUT, POST, DELETE, PATCH",
		RequestHeaders:  "Origin, Authorization, Content-Type",
		ExposedHeaders:  "",
		MaxAge:          50 * time.Second,
		Credentials:     false,
		ValidateHeaders: false,
	}))
	log.Println("Adding routes to restAPI server.")
	router.GET("/goals/", todopkg.GetToDo)
	router.POST("/goals/", todopkg.GenerateToDo)
	router.GET("/goals/:id", todopkg.GetToDo)
	router.DELETE("/goals/:id", todopkg.DeleteToDo)
	router.PATCH("/goals/:id", todopkg.UpdateToDo)
	router.Run("localhost:8000")
	log.Println("restAPI server started on port 8000.")
}
