/*
This is database setup package file
It opents connection to sqlite3 database
It performs the migration through automigration
*/

package db

import (
	"log"
	constants "todo/src/const"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var Connection *gorm.DB

func ConnectDatabase() {
	log.Println("Connecting to database")
	database, err := gorm.Open("sqlite3", "myTodo.db")

	if err != nil {
		errMsg := "Failed to connect to database!"
		log.Panic(errMsg, err)
		panic(errMsg)
	}

	database.AutoMigrate(&(constants.ToDoDBStruct{}))

	Connection = database
}
